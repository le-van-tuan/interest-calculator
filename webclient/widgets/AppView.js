function AppView() {
    BaseApplicationView.call(this);

    this.bind("click", this.calculateInterest, this.calculateBtn);
    this.bind("input", (e) => {
        if (this.interval) clearTimeout(this.interval);
        this.interval = setTimeout(() => {
            this.onInputChangedHandler(e);
        }, 200);
    }, this.loanAmount);

    this.bind("click", () => {
        API.download("/api/excel", this.lastResult);
    }, this.exportExcelBtn);
}

__extend(BaseApplicationView, AppView);

AppView.prototype.onAttached = function () {
    this.dataTable.column(
        new DataTable.PlainTextColumn("Month", function (data, row, col) {
            return row;
        }).width("60px")
    );
    this.dataTable.column(
        new DataTable.PlainTextColumn("Dư nợ", function (data) {
            return new Intl.NumberFormat().format(data.remainingAmount);
        }).width("160px")
    );
    this.dataTable.column(
        new DataTable.PlainTextColumn("Gốc trả", function (data) {
            return new Intl.NumberFormat().format(data.monthyAmount);
        }).width("160px")
    );
    this.dataTable.column(
        new DataTable.PlainTextColumn("Lãi xuất (%)", function (data) {
            return data.interest + "%";
        }).width("160px")
    );
    this.dataTable.column(
        new DataTable.PlainTextColumn("Lãi (vnđ)", function (data) {
            return new Intl.NumberFormat().format(data.monthlyInterest);
        }).width("160px")
    );
    this.dataTable.column(
        new DataTable.PlainTextColumn("Số tiền trả hàng tháng", function (data) {
            return new Intl.NumberFormat().format(parseFloat(data.totalEachMonth.toFixed(2)));
        }).width("160px")
    );
    this.dataTable.setup();
};

AppView.prototype.onInputChangedHandler = function (e) {
    const value = e.target.value;
    let converted = value.toString().replace(/[^\d.-]+/g, '');
    if (isNaN(converted)) {
        this.loanAmount.value = this.lastGoodValue || "";
        return;
    }

    let formattedValue = new Intl.NumberFormat().format(converted);
    this.lastGoodValue = formattedValue;
    this.loanAmount.value = this.lastGoodValue;
}

AppView.prototype.calculateInterest = function () {
    var thiz = this;
    API.post("/api/calculate", {
        loanAmount: parseInt(thiz.loanAmount.value.toString().replace(/[^\d.-]+/g, '')),
        promotionMonth: parseInt(thiz.promotionMonth.value),
        promotionInterest: parseInt(thiz.promotionInterest.value),
        loanMonth: parseInt(thiz.loanMonth.value),
        loanInterest: parseInt(thiz.loanInterest.value),
        interuptMonth: parseInt(thiz.interuptMonth.value)
    }).then(function (result) {
        thiz.dataTable.setItems(result.details);
        Dom.setInnerText(thiz.totalLoanAmount, `${new Intl.NumberFormat().format(result.loanAmount)} vnđ`);
        Dom.setInnerText(thiz.totalLoanMonth, new Intl.NumberFormat().format(result.totalLoanMonth || 0));
        Dom.setInnerText(thiz.interestAmount, `${new Intl.NumberFormat().format(result.totalInterestAmount || 0)} vnđ`);
        Dom.setInnerText(thiz.totalAmount, `${new Intl.NumberFormat().format(result.totalAmount || 0)} vnđ`);
        Dom.setInnerText(thiz.totalInteruptAmount, `${new Intl.NumberFormat().format(result.totalInterestAtInteruptMonth || 0)} vnđ`);

        thiz.lastResult = result;
        thiz.exportExcelBtn.removeAttribute("disabled");
    });
}
