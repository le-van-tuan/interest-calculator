function StyledInput(node) {
    BaseWidget.call(this);

    this.prefixIcon = Dom.getAttributeAsString(node, "prefix-icon", "magnify");
    this.suffixIcon = Dom.getAttributeAsString(node, "suffix-icon", "close-circle-outline");
    this.usePrefix = Dom.getAttributeAsBoolean(node, "use-prefix", true);
    this.useSuffix = Dom.getAttributeAsBoolean(node, "use-suffix", false);
    this.placeholder = Dom.getAttributeAsString(node, "placeholder", "");
    this.populateWidgets();

    this.bind("input", this.inputChangedHandler, this.input);
    this.bind("click", this.suffixClickedHandler, this.suffix);
}
__extend(BaseTemplatedWidget, StyledInput);

StyledInput.prototype.getValue = function() {
    return this.input.value;
};

StyledInput.prototype.inputChangedHandler = function (e) {
    Dom.toggleClass(this.node(), "Inputted", e.target?.value);
};

StyledInput.prototype.suffixClickedHandler = function (e) {
    this.input.value = "";
    this.input.focus();
    Dom.removeClass(this.node(), "Inputted");
};

StyledInput.prototype.getFocusNode = function () {
    return this.input;
};

StyledInput.prototype.setPlaceholder = function (value) {
    this.input.placeholder = value;
};

StyledInput.prototype.populateWidgets = function () {
    this.input.placeholder = this.placeholder;
    Dom.addClass(this.prefix, this.prefixIcon);
    Dom.addClass(this.suffix, this.suffixIcon);

    Dom.toggleClass(this.prefix, "Applicable", this.usePrefix);
    Dom.toggleClass(this.suffix, "Applicable", this.useSuffix);
};