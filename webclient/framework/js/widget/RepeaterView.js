function RepeaterView(definitionNode) {
    BaseWidget.call(this, definitionNode);
    this.fakeItemCount = Dom.getAttributeAsInt(definitionNode, "fake-item-count", 0);
    this.itemTemplateMap = {};
}
__extend(BaseWidget, RepeaterView);

RepeaterView.prototype.buildDOMNode = function (definitionNode) {
    var nodeName = "hbox";
    if (definitionNode) {
        nodeName = definitionNode.getAttribute("tag") || nodeName;
    }

    return document.createElement(nodeName);
};
RepeaterView.prototype.setContentFragment = function (fragment) {
    var index = 1;
    for (var i = 0; i < fragment.childNodes.length; i ++) {
        var node = fragment.childNodes[i];
        if (!node.getAttribute) continue;

        var role = node.getAttribute("role");
        if (role == "header") {
            this.headerTemplate = node;
        } else if (role == "footer") {
            this.footerTemplate = node;
        } else if (role == "empty") {
            this.emptyTemplate = node;
        } else {
            this.itemTemplate = node;
            this.itemTemplateMap[role] = node;
        }
    }
};

RepeaterView.prototype.onAttached = function () {
    Dom.emitEvent("p:onAttached", this.node());
};

RepeaterView.prototype.createBinding = function (container) {
    container._binding = {};
    container._binding._node = container;
    widget.Util.performAutoBinding(container, container._binding, null, "forced");

    Dom.doOnChildRecursively(container, {
        eval: function(n) {
            return n.getAttribute && n.getAttribute("anon-id");
        }
    }, function(n) {
        var id = n.getAttribute("anon-id");

        if (container._binding) {
            container._binding[id] = n;
        }
        n.removeAttribute("anon-id");

        var newId = id + widget.random();
        n.setAttribute("id", newId);
        n.id = newId;
        Dom.addClass(n, "AnonId_" + id);
    });
}


RepeaterView.prototype.generate = function (container, templateNode, data, index) {
    var node = templateNode.cloneNode(true);
    this.createBinding(node);

    container.appendChild(node);

    if (this.populator && typeof(data) != "undefined") this.populator(data, node._binding, node, index);
    node.setAttribute("data-index", index);

    node._repeaterData = data;

    return node;
};
RepeaterView.prototype.getItems = function() {
    return this.items;
}
RepeaterView.prototype.setItems = function (items, doneCallback) {
    this._prepareView();

    if (!items || items.length <= 0) {
        if (this.emptyTemplate) {
            Dom.empty(this.node());
            this._itemContainerNode = null;
            this.node().appendChild(this.generate(this.node(), this.emptyTemplate));
        }

        if (doneCallback && typeof(doneCallback) == "function") doneCallback.apply(this);
        return;
    }
    var container = this.node();
    if (this["content-wrapper-tag"]) {
        container = document.createElement(this["content-wrapper-tag"]);
        Dom.addClass(container, "ContentWrapper");

        this.node().appendChild(container);
    }

    this._itemContainerNode = container;

    this.appendItems(items);


    if (this.footerTemplate) {
        this.generate(this.node(), this.footerTemplate);
    }

    if (doneCallback && typeof(doneCallback) == "function") doneCallback.apply(this);
};

/* Renderer API - this is to support pagination via either standard or infinite scroll paginators*/
RepeaterView.prototype.withItemComparer = function (comparer) {};
RepeaterView.prototype.invalidateSelectionInfo = function () {};
RepeaterView.prototype.markAppending = function (appending) {
    Dom.toggleClass(this.node(), "RepeaterViewAppending", appending);
};
RepeaterView.prototype.withItemComparer = function () {};

RepeaterView.prototype.addListener = function (listener) {
    if (!this._listeners) {
        this._listeners = [];

        var thiz = this;

        this.node().addEventListener("scroll", function () {
            var scrollPane = thiz.node();
            var dy = Math.max(0, scrollPane.scrollHeight - (scrollPane.scrollTop + scrollPane.clientHeight));
            thiz._listeners.forEach(function (listener) {
                if (typeof(listener.onVerticalScrolled) == "function") {
                    listener.onVerticalScrolled(dy);
                }
            })
        }, false);
    }
    this._listeners.push(listener);
};
RepeaterView.prototype._prepareView = function () {
    this.items = [];
    Dom.empty(this.node());

    if (this.headerTemplate) {
        this.generate(this.node(), this.headerTemplate);
    }

    var container = this.node();
    if (this["content-wrapper-tag"]) {
        container = document.createElement(this["content-wrapper-tag"]);
        Dom.addClass(container, "ContentWrapper");

        this.node().appendChild(container);

        if (this.footerTemplate) {
            this.generate(this.node(), this.footerTemplate);
        }
    }

    this._itemContainerNode = container;
    // console.log("  _prepareView with container = ", container);
};
RepeaterView.prototype.appendItems = function (items) {
    // console.log("appendItems() called", this._itemContainerNode);
    if (this.onBeforeAppending) this.onBeforeAppending();

    var startIndex = this.items ? this.items.length : 0;
    this.items = (this.items || []).concat(items);

    if (!this._itemContainerNode) this._prepareView();
    var container = this._itemContainerNode;

    while (container.lastChild && container.lastChild._repeaterData && container.lastChild._repeaterData._fake) {
        container.removeChild(container.lastChild);
    }
    
    if (this.itemTemplate) {
        for (var i = 0; i < items.length; i ++) {
            var template = this.itemTemplate;
            if (this.getTemplateRoleForData) {
                var role = this.getTemplateRoleForData(items[i], startIndex + i);
                if (role && this.itemTemplateMap[role]) template = this.itemTemplateMap[role];
            }
            
            this.generate(container, template, items[i], startIndex + i);
        }

        if (this.fakeItemCount) {
            for (var i = 0; i < this.fakeItemCount; i ++) {
                var fakeData = {_fake: true};
                
                var template = this.itemTemplate;
                if (this.getTemplateRoleForData) {
                    var role = this.getTemplateRoleForData(fakeData, startIndex + items.length + i);
                    if (role && this.itemTemplateMap[role]) template = this.itemTemplateMap[role];
                }
                
                this.generate(container, template, fakeData, startIndex + items.length + i);
            }
        }
    }
    if (this.onAfterAppending) this.onAfterAppending();
};

RepeaterView.prototype.isNearEnd = function () {
    var scrollPane = this.node();
    var dy = Math.max(0, scrollPane.scrollHeight - (scrollPane.scrollTop + scrollPane.clientHeight));
    return dy < (this._nearEndGap || (20 * Util.em()));
};

RepeaterView.prototype.reset = function (items) {
    // console.trace();
    // console.log("reset() called");
    this._prepareView();
    // console.log("   _prepareView() done.");
};


RepeaterView.prototype.deleteRow = function(contextNode) {
    var nodeContext = RepeaterView._getNodeContext(contextNode);
    if (!nodeContext) return null;

    if (this != nodeContext.instance) return;
    var next = nodeContext.row.nextElementSibling;
    while (next) {
        if (next.hasAttribute("data-index")) {
            var index = parseInt(next.getAttribute("data-index"), 10);
            next.setAttribute("data-index", index - 1);
        }

        next = next.nextElementSibling;
    }
    this.items.splice(nodeContext.index, 1);
    nodeContext.row.parentNode.removeChild(nodeContext.row);
};
RepeaterView._getNodeContext = function (node) {
    var repeater = RepeaterView.findInstanceFromNode(node);
    if (!repeater) return null;

    var row = Dom.findUpward(node, {
        eval: function (n) {
            return n.getAttribute && n.getAttribute("role") == "item";
        }
    });

    if (!row) return null;

    var index = parseInt(row.getAttribute("data-index"), 10);
    var data = repeater.items[index];

    return {
        instance: repeater,
        index: index,
        row: row,
        data: data
    };
};
RepeaterView.findInstanceFromNode = function (target) {
    var repeater = Dom.findUpward(target, {
        eval: function (n) {
            return n.__widget;
        }
    });

    if (!repeater) return null;

    return repeater.__widget;
};