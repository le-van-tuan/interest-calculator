var InfiniteScrollPaginator = function () {
    function InfiniteScrollPaginator() {
        this.renderer = null;
        this.source = null;
        this.pageSize = 40;
        this.lastLoadedIndex = -1;
        this.totalPages = 0;
        this.firstRender = true;

        this.selectedItems = [];
    }

    InfiniteScrollPaginator.prototype.setup = function (options) {
        this.options = options ? options : null;
        this.comparer = (options && options.comparer) ? options.comparer : function (a, b) {
            return a.id == b.id;
        };
    };
    InfiniteScrollPaginator.prototype.control = function (renderer) {
        if (!renderer.appendItems || !renderer.addListener) throw new Error("Renderer must support .appendItems() and .addListener()");
        this.renderer = renderer;
        this.renderer.withItemComparer(this.comparer);

        if (this.renderer.addOrderRequestListener) {
            this.renderer.addOrderRequestListener(this);
        }

        var thiz = this;

        var listener = {
            onSelectionChanged: function (table, fromUserAction) {
                thiz.onRendererSelectionChanged(table, fromUserAction);
                thiz.renderer.invalidateSelectionInfo(thiz);
            },
            onSelectNoneAction: function () {
                thiz.clearSelection();
            },
            onSizeChanged: function () {
                thiz.onRendererSizeChanged();
            },
            onVerticalScrolled: function (dy) {
                thiz.handleRendererInfiniteScrollDown(dy);
            }
        };
        
        this.renderer.addListener(listener);

        // var showInfiniteScrollPaginator = this.options != null && this.options.showInfiniteScrollPaginator ? true : false;
        // 
        // if (showInfiniteScrollPaginator) {
        //     if (this.renderer.markedAsPaginated) this.renderer.markedAsPaginated(true);
        //     return;
        // }
        //console.log("add load more event " + showInfiniteScrollPaginator);
        // Dom.registerEvent(this.renderer.container, "scroll", function(e){
        //     var position = this.scrollTop + this.offsetHeight;
        //     var percent = position / this.scrollHeight * 100;
        //     //console.log(percent);
        //     if (percent >= 75) {
        //         thiz.loadMore();
        //     }
        // });
    };
    InfiniteScrollPaginator.prototype.clearSelection = function () {
        this.selectedItems = [];
        this.renderer.invalidateSelectionInfo(this);
        this.fireSelectionChangedEvent();
    };

    // InfiniteScrollPaginator.prototype.loadMore = function() {
    //     //console.log("load");
    //     var thiz = this;
    //     this.currentPage++;
    //     this.source.loadPage(this.currentPage, this.pageSize, function (results, totalItems) {
    //         if (thiz.pageSize < results.length) {
    //             thiz.renderer.addItems(results.slice(0, thiz.pageSize));
    //         } else {
    //             thiz.renderer.addItems(results);
    //         }
    //     });
    // };

    InfiniteScrollPaginator.prototype.onRendererSelectionChanged = function (table, fromUserAction) {
        if (!fromUserAction) return;

        if (this.renderer.isSelectAll()) {
            this.selectedItems = [];
        } else {
            var items = this.renderer.getSelectedItems();
            var all = this.renderer.items;

            if (!all) return;

            var newSelectedItems = [];
            for (var i = 0; i < this.selectedItems.length; i ++) {
                var item = this.selectedItems[i];
                if (!Util.contains(all, item, this.comparer) || Util.contains(items, item, this.comparer)) {
                    newSelectedItems.push(item);
                }
            }
            for (var i = 0; i < items.length; i ++) {
                var item = items[i];
                if (!Util.contains(this.selectedItems, item, this.comparer)) {
                    newSelectedItems.push(item);
                }
            }

            this.selectedItems = newSelectedItems;
        }

        this.fireSelectionChangedEvent();
    };

    InfiniteScrollPaginator.prototype.fireSelectionChangedEvent = function (fromUserAction) {
        if (this.onSelectionChanged) this.onSelectionChanged(this, fromUserAction);
    };

    InfiniteScrollPaginator.prototype.changeOrder = function (order) {
        if (!this.source || !this.source.setOrder) return;
        this.source.setOrder(order);
        this._reloadFromStart();
    };
    InfiniteScrollPaginator.prototype.onRendererSizeChanged = function () {
    };
    
    InfiniteScrollPaginator.prototype._loadMore = function () {
        if (this.loadingInprogress) return;
        var thiz = this;
        this.loadingInprogress = true;
        this.renderer.markAppending(true);
        
        
        var count = 4;
        var load = function () {
            thiz._loadNextPage(function (successful) {
                if (!thiz._shouldLoadMore() || !thiz._isMoreItemsAvailable() || --count <= 0) {
                    thiz.loadingInprogress = false;
                    thiz.renderer.markAppending(false);
                    return;
                }
                
                load();
            });
        };
        
        load();
    };
    InfiniteScrollPaginator.prototype._createResultHandler = function (index, callback) {
        var thiz = this;
        return {
            onSuccess: function (results, totalItems) {
                if (this._destroyed) return;
                try {
                    thiz.renderer.appendItems(results);
                } finally {
                    thiz.total = totalItems;
                    thiz.totalItems = totalItems;
                    thiz.lastLoadedIndex = index;
                }
                
                try {
                    if (thiz.options && thiz.options.onPageLoaded) {
                        thiz.options.onPageLoaded(thiz, results.length);
                    }
                } catch (e) {
                    console.error(e);
                }
                
                if (thiz.source.getOrder && thiz.renderer.setOrder) {
                    thiz.renderer.setOrder(thiz.source.getOrder());
                }
                
                callback(true);
            },
            onFailure: function () {
                if (this._destroyed) return;
                try {
                    if (thiz.options && thiz.options.onPageLoadFailed) {
                        thiz.options.onPageLoadFailed(index);
                    }
                } catch (e) {
                    console.error(e);
                }
                
                callback(false);
            },
            destroy: function () {
                this._destroyed = true;
            }
        };
    };

    InfiniteScrollPaginator.prototype._loadNextPage = function (callback) {
        var index = this.lastLoadedIndex + 1;
        var thiz = this;
        
        if (thiz.options && thiz.options.onPageStartLoading) {
            thiz.options.onPageStartLoading(index);
        }
        
        if (this._resultHandler) {
            this._resultHandler.destroy();
        }
        
        this._resultHandler = this._createResultHandler(index, callback);
        
        this.source.loadPage(index, this.pageSize, this._resultHandler.onSuccess, this._resultHandler.onFailure);
    };
    
    InfiniteScrollPaginator.prototype._shouldLoadMore = function () {
        if (!this.source) return false;
        
        var now = new Date().getTime();
        if (this.renderer.isNearEnd()) {
            if (!this._isMoreItemsAvailable()) {
                if (!this.forceReloadIfAged) return false;
                
                var lastCheck = this.lastLoadMoreCheck || 0;
                if (now - lastCheck < 10000) return false;
                
                this.lastLoadMoreCheck = now;
            } else {
                this.lastLoadMoreCheck = 0;
            }

            return true;
        }
        return false;
    };
    InfiniteScrollPaginator.prototype._isMoreItemsAvailable = function () {
        return this.lastLoadedIndex < 0 || ((this.lastLoadedIndex + 1) * this.pageSize < this.total);
    };

    InfiniteScrollPaginator.prototype._reloadFromStart = function () {
        this.lastLoadedIndex = -1;
        this.firstRender = true;
        if (this.renderer) {
            if (this.renderer.reset) this.renderer.reset();
            this.renderer.setItems([], true);
            this._loadMore();
        }
    };

    InfiniteScrollPaginator.prototype.setSource = function (source) {
        this.source = source;
        this._allItems = null;
        this.selectedItems = [];
        this._reloadFromStart();
    };

    InfiniteScrollPaginator.prototype.refresh = function (withIndicator) {
        if (!this.source) return;
        this._allItems = null;
        this._reloadFromStart();
    };

    InfiniteScrollPaginator.prototype.reInit = function () {
        if (this.firstRender) {
            this.invalidateRendererHeight();
            this.renderer.invalidateSizing();
            return;
        }

        this._reloadFromStart();
    };

    InfiniteScrollPaginator.prototype.getSelectionCount = function () {
        if (!this.renderer) return 0;
        if (!this.renderer.isSelectAll()) {
            return this.selectedItems.length;
        } else {
            return this.totalItems - (this.renderer._getExcludedItemCounts ? this.renderer._getExcludedItemCounts() : 0);
        }
    };
    InfiniteScrollPaginator.prototype.getSelectedItems = function (callback, forceSelectAll) {
        if (!callback) {
            return this.selectedItems;
        }
        if ((typeof(forceSelectAll) == "undefined" || forceSelectAll == null) && !this.renderer.isSelectAll()) {
            callback(this.selectedItems);
        } else {
            if (this.source == null) {
                callback([]);
                return;
            }
            
            var total = this.totalItems;
            var chunk = 100;
            var items = [];
            var thiz = this;
            var index = 0;
            var next = function () {
                thiz.source.loadPage(index, chunk, function (results, totalItems) {
                    var excludedCount = 0;
                    for (var i = 0; i < results.length; i ++) {
                        if (thiz.renderer._getExcludedItemCounts && thiz.renderer._getExcludedItemCounts() > 0) {
                            if (thiz.renderer.isIdUnselected(results[i].id)) {
                                excludedCount ++;
                                continue;
                            }
                        }

                        items.push(results[i]);
                    }
                    
                    total = totalItems - excludedCount;
                    index ++;
                    if (items.length < total && results.length > 0) {
                        next();
                    } else {
                        items = items.filter(function (data) {
                            return !thiz.renderer.selectable || thiz.renderer.selectable(data);
                        });
                        defaultIndicator.done();
                        //console.log("Done loading " + items.length + " items.");
                        thiz._allItems = items;
                        callback(items);
                    }
                }, function (error) {
                    console.error(error);
                    defaultIndicator.done();
                });
            };
            defaultIndicator.busy("Fetching data list...");
            next();
        }
    };
    
    InfiniteScrollPaginator.prototype.getSelectedItemIds = function (callback) {
        if (!callback) {
            return this.selectedItems.map(function (i) { return i.id; });
        }
        if (!this.renderer.isSelectAll()) {
            callback(this.selectedItems ? (this.selectedItems.map(function (i) { return i.id; })) : []);
        } else {
            if (this.source == null) {
                callback([]);
                return;
            }
            if (this._allItems) {

                callback(this._allItems.map(function (i) { return i.id; }));
                return;
            }
            
            if (!this.source.loadAllIds) throw "Source does not support loadAllIds";
            var thiz = this;
            this.source.loadAllIds(function (ids) {
                var filterIds = ids;
                if (thiz.renderer._getExcludedItemCounts && thiz.renderer._getExcludedItemCounts() > 0) {
                    filterIds = ids.filter(function (id) {
                        return !thiz.renderer.isIdUnselected(id);
                    });
                }
                callback(filterIds);
            });
        }
    };
    
    
    InfiniteScrollPaginator.prototype.handleRendererInfiniteScrollDown = function (dy) {
        if (this._shouldLoadMore()) this._loadMore();
    };


    return InfiniteScrollPaginator;
}();
