function CurrencyInput(node) {
    BaseTemplatedWidget.call(this);
    var readOnly = Dom.getAttributeAsBoolean(node, "read-only", false);
    if (readOnly) this.currencyInput.readOnly = true;

    this.valueTitle = Dom.getAttributeAsString(node, "title", "value");
    this.min = Dom.getAttributeAsFloat(node, "min", 0);
    this.max = Dom.getAttributeAsFloat(node, "max", 0);

    this.exclusiveMin = Dom.getAttributeAsBoolean(node, "exclusive-min", false);
    this.exclusiveMax = Dom.getAttributeAsBoolean(node, "exclusive-max", false);

    this.bind("click", this.amountIconTypeClickedHandler, this.amountIconType);
    this.bind("click", this.switchToDollarMode, this.amountTypeDollar);
    this.bind("click", this.switchToPercentMode, this.amountTypePercent);

    this.AMOUNT_TYPE_DOLLAR = "dollar";
    this.AMOUNT_TYPE_PERCENT = "percent";
    this.AMOUNT_TYPE_VND = "vnd";
    this.amountType = Dom.getAttributeAsString(node, "amount-type", this.AMOUNT_TYPE_DOLLAR);
    this.defaultValue = Dom.getAttributeAsString(node, "default-value", "");
    this.onlyPositive = Dom.getAttributeAsString(node, "only-positive", false);

    this.inputDisplayRenderer = function(inputVal) {
        if (typeof(inputVal) == "undefined" || inputVal.length == 0 || isNaN(inputVal)) {
            if (this.currencyInput._lastGoodValue) {
                return Util.toCurrency(CurrencyInput.parseInput(this.currencyInput._lastGoodValue), {noSymbol: true});
            }
            return this.defaultValue;
        }
        return Util.toCurrency(CurrencyInput.parseInput(inputVal), {noSymbol: true});
    }

    this.bind("input", function (event) {
        var number = CurrencyInput.parseInput(this.currencyInput.value);
        var isInvalidInput = isNaN(number) || (this.onlyPositive && number < 0);
        if (!isInvalidInput) {
            this.currencyInput._lastGoodValue = CurrencyInput.parseInput(this.currencyInput.value);
            var thiz = this;
            setTimeout(function () {
                var displayVal = Util.toCurrency(thiz.currencyInput._lastGoodValue, {noSymbol: true});
                thiz.currencyInput.value = displayVal;
            }, 200);
            this._fireValueChangedEvent();
        } else {
            this.currencyInput.value = Util.toCurrency(this.currencyInput._lastGoodValue, {noSymbol: true});;
        }
    }, this.currencyInput);
}

__extend(BaseTemplatedWidget, CurrencyInput);

CurrencyInput.parseInput = function(input) {
    return Number((input || "0").replace(/\./g, ""));
};
CurrencyInput.prototype.onAttached = function() {
    if (this.amountType == this.AMOUNT_TYPE_DOLLAR) {
        Dom.addClass(this.node(), "AmountTypeDollar");
        Dom.removeClass(this.node(), "AmountTypePercent");
        Dom.removeClass(this.node(), "AmountTypeVND");
    } else if (this.amountType == this.AMOUNT_TYPE_PERCENT) {
        Dom.removeClass(this.node(), "AmountTypeDollar");
        Dom.addClass(this.node(), "AmountTypePercent");
        Dom.removeClass(this.node(), "AmountTypeVND");
    } else if (this.amountType == this.AMOUNT_TYPE_VND) {
        Dom.addClass(this.node(), "AmountTypeVND");
        Dom.removeClass(this.node(), "AmountTypePercent");
        Dom.removeClass(this.node(), "AmountTypeDollar");
    }
};

CurrencyInput.prototype.setValue = function (input) {
    if (isNaN(input)) {
        this.currencyInput.value = this.defaultValue;
        this.currencyInput._lastGoodValue = this.defaultValue;
    } else {
        this.currencyInput.value = Util.toCurrency(input, {noSymbol: true});
        this.currencyInput._lastGoodValue = input;
    }
}

CurrencyInput.prototype.getValue =  function () {
    return Number(CurrencyInput.parseInput(this.currencyInput.value));
};

CurrencyInput.prototype._fireValueChangedEvent = function () {
    Dom.emitEvent("p:CurrencyValueChanged", this.node(), {value: this.getValue()});
};

CurrencyInput.prototype._setCurrencyInputValue = function (value) {
    if (typeof(value) == "undefined") {
        this.currencyInput.value = this.defaultValue;
        this.currencyInput._lastGoodValue = this.defaultValue;
        this._modifiedByUser = false;
        return;
    }
    if (this.amountType == this.AMOUNT_TYPE_PERCENT) {
        this.currencyInput.value = value;
        this.currencyInput._lastGoodValue = value;
    } else {
        this.currencyInput.value = Util.toCurrency(value, {noSymbol: true});
        this.currencyInput._lastGoodValue = Util.toCurrency(value, {noSymbol: true});
    }

    this._modifiedByUser = false;
};

CurrencyInput.prototype.getValidationRuleSet = function () {
    var message =  "Please enter a valid " + this.valueTitle;
    if (!isNaN(this.min) && !isNaN(this.max)) {
        message += " (" + this.min + " to " + this.max + ")";
    } else if (!isNaN(this.min)){
        message += " (min: " + this.min + ")";
    } else if (!isNaN(this.max)) {
        message += " (max: " + this.max + ")";
    }

    message += ".";

    return new RuleSet()
        .live(true)
        .number(this.currencyInput, {min: this.min, max: this.max, required: true, exclusiveMin: this.exclusiveMin, exclusiveMax: this.exclusiveMax}, message);
};

CurrencyInput.prototype.setEnable = function (enable) {
    this.currencyInput.disabled = !enable;
    Dom.toggleClass(this.node(), "Disabled", !enable);
};

CurrencyInput.prototype.setEnableSwitchMode = function (enable) {
    this.enabledSwicthMode = enable;
    Dom.toggleClass(this.node(), "EnabledSwitchMode", enable);
};

CurrencyInput.prototype.amountIconTypeClickedHandler = function () {
    if (!this.enabledSwicthMode) return;
    if (this.currencyInput.readOnly) return;
    if (this.currencyInput.disabled) return;
    this.amountTypePopup.show(this.amountIconType, "left-inside", "bottom", 0, 0, true);
};

CurrencyInput.prototype.switchToDollarMode = function () {
    this.amountTypePopup.hide();
    if (this.amountType == this.AMOUNT_TYPE_DOLLAR) return;
    this.amountType = this.AMOUNT_TYPE_DOLLAR;
    Dom.addClass(this.node(), "AmountTypeDollar");
    Dom.removeClass(this.node(), "AmountTypePercent");
    Dom.emitEvent("p:CurrencyTypeChanged", this.node());
};

CurrencyInput.prototype.switchToPercentMode = function () {
    this.amountTypePopup.hide();
    if (this.amountType == this.AMOUNT_TYPE_PERCENT) return;
    this.amountType = this.AMOUNT_TYPE_PERCENT;
    Dom.removeClass(this.node(), "AmountTypeDollar");
    Dom.addClass(this.node(), "AmountTypePercent");
    Dom.emitEvent("p:CurrencyTypeChanged", this.node());
};

CurrencyInput.prototype.isPercentMode = function () {
    return this.amountType == this.AMOUNT_TYPE_PERCENT;
};

CurrencyInput.prototype.setPlaceHolder = function (text) {
    this.currencyInput.setAttribute("placeholder", Util.toCurrency(text, {noSymbol: true}));
};

CurrencyInput.prototype.getFocusNode = function () {
    return this.currencyInput;
};
