function ScrollbarlessScrollView () {
    BaseTemplatedWidget.call(this);
    
    this.bind("scroll", this._invalidateScrollbar, this.container);

    var thiz = this;
    
    Dom.onResize(this.container, function () { thiz._invalidateScrollbar(); });
    Dom.onResize(this.container, function () { thiz._invalidateScrollbar(); }, true);
    
    this.bind("mousedown", function (e) {
        ScrollbarlessScrollView._heldInstance = thiz;
        ScrollbarlessScrollView._lastY = e.screenY;
        ScrollbarlessScrollView._lastOffset = Math.round(thiz.container.scrollTop);
        ScrollbarlessScrollView._lastFactor = thiz.container.scrollHeight / thiz.container.offsetHeight;
        Dom.addClass(thiz.node(), "ThumbHeld");
        Dom.cancelEvent(e);
    }, this.thumb);
    
    if (!ScrollbarlessScrollView._globalListenersInstalled) {
        document.addEventListener("mousemove", ScrollbarlessScrollView._handleMouseMove, false);
        document.addEventListener("mouseup", function () {
            if (ScrollbarlessScrollView._heldInstance) {
                Dom.removeClass(ScrollbarlessScrollView._heldInstance.node(), "ThumbHeld");
            }
            ScrollbarlessScrollView._heldInstance = null;
        }, false);

        ScrollbarlessScrollView._globalListenersInstalled = true;
    }
}

__extend(BaseTemplatedWidget, ScrollbarlessScrollView);
ScrollbarlessScrollView._handleMouseMove = function (event) {
    if (!ScrollbarlessScrollView._heldInstance) return;
    
    Dom.cancelEvent(event);
    
    var thiz = ScrollbarlessScrollView._heldInstance;
    var dy = event.screenY - ScrollbarlessScrollView._lastY;
    var maxOffset = thiz.container.scrollHeight - thiz.container.offsetHeight;
    var offset = ScrollbarlessScrollView._lastOffset + dy * ScrollbarlessScrollView._lastFactor;
    offset = Math.max(0, Math.min(maxOffset, offset));
    
    thiz.container.scrollTop = offset;
};
ScrollbarlessScrollView.prototype.setContentFragment = function (fragment) {
    this.container.appendChild(fragment);
};

ScrollbarlessScrollView.prototype._invalidateScrollbar = function () {
    var h = Math.round(this.container.offsetHeight);
    var H = Math.round(this.container.scrollHeight);
    var offset = Math.round(this.container.scrollTop);
    
    var isVertcalOverflowed = H > h;
    Dom.toggleClass(this.node(), "VerticalOverflowed", isVertcalOverflowed);
    if (isVertcalOverflowed) {
        this.thumb.style.top = (100 * offset / H) + "%";
        this.thumb.style.height = (100 * h / H) + "%";
    }
};