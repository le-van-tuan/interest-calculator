var Utils = {
    time2Text: function (seconds) {
        let hour = Math.floor(seconds / 3600) || 0;
        seconds %= 3600;
        let min = Math.floor(seconds / 60) || 0;
        let sec = (seconds %= 60);

        return `${hour ? hour + ":" : ""}${min ? min + ":" : ""}${sec}`;
    }
}