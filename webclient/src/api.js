var API = {
    get: function (path, params) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.addEventListener("load", function () {
                if (request.status != 200) {
                    reject(new Error("Response = " + request.status));
                    return;
                }

                var json = request.responseText;
                var object = JSON.parse(json);
                resolve(object);
            });
            var url = path;
            if (params) {
                for (var name in params) {
                    url += url.indexOf("?") < 0 ? "?" : "&";
                    url += name + "=";
                    url += encodeURIComponent("" + params[name]);
                }
            }

            request.open("GET", url);
            request.send();
        });
    },
    post: function (path, body) {
        return new Promise(function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.addEventListener("load", function () {
                if (request.status != 200) {
                    reject(new Error("Response = " + request.status));
                    return;
                }

                var json = request.responseText;
                var object = JSON.parse(json);
                resolve(object);
            });
            var url = path;

            request.open("POST", url);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(body));
        });
    },
    download: function (path, body) {
        axios({
            url: path,
            method: "POST",
            responseType: "blob",
            data: body
        }).then((response) => {
            const href = URL.createObjectURL(response.data);

            const link = document.createElement("a");
            link.href = href;
            link.setAttribute("download", "bangtinh.xlsx");
            document.body.appendChild(link);
            link.click();

            document.body.removeChild(link);
            URL.revokeObjectURL(href);
        });
    },
};
