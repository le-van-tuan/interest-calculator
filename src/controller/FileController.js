const excelJS = require("exceljs");

const export2Excel = async (req, res) => {
    const body = req.body;

    let workbook = new excelJS.Workbook();
    let worksheet = workbook.addWorksheet("Bảng Tính", {properties: {tabColor: {argb: "84c55f"}}});

    worksheet.columns = [
        {header: "Tháng", key: "index", width: 10},
        {header: "Dư nợ", key: "remainingAmount", width: 30},
        {header: "Gốc trả", key: "monthyAmount", width: 30},
        {header: "Lãi xuất (%)", key: "interest", width: 15},
        {header: "Lãi (vnđ)", key: "monthlyInterest", width: 30},
        {header: "Số tiền trả hàng tháng", key: "totalEachMonth", width: 30},
    ];

    worksheet.getRow(1).eachCell((cell) => {
        cell.font = {bold: true};
    });

    body.details.forEach((e, index) => {
        worksheet.addRow({
            ...e,
            index: index,
        });
    });

    worksheet.getColumn("index").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "center"};
    });
    worksheet.getColumn("remainingAmount").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "right"};
        if (rowNumber > 1) {
            cell.value = new Intl.NumberFormat().format(cell.value);
        }
    });
    worksheet.getColumn("monthyAmount").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "right"};
        if (rowNumber > 1) {
            cell.value = new Intl.NumberFormat().format(cell.value);
        }
    });
    worksheet.getColumn("interest").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "right"};
        if (rowNumber > 1) {
            cell.value = cell.value + "%";
        }
    });
    worksheet.getColumn("monthlyInterest").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "right"};
        if (rowNumber > 1) {
            cell.value = new Intl.NumberFormat().format(cell.value);
        }
    });
    worksheet.getColumn("totalEachMonth").eachCell(function (cell, rowNumber) {
        cell.alignment = {horizontal: "right"};
        if (rowNumber > 1) {
            cell.value = new Intl.NumberFormat().format(cell.value);
        }
    });

    res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    res.setHeader("Content-Disposition", "attachment; filename=" + "BangTinh.xlsx");

    return workbook.xlsx.write(res).then(() => res.status(200).end());
};

module.exports = export2Excel;
