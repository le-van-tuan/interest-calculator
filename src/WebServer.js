(function () {
    const express = require("express");
    const path = require("path");
    const addWSSupport = require("express-ws");
    const fs = require("fs");

    const export2Excel = require("./controller/FileController");

    var server = null;
    var wsServer = null;
    var exportedServer = {
        start: start,
    };
    function start() {
        server = express();
        wsServer = addWSSupport(server);

        server.use(express.json());
        server.use(function (req, res, next) {
            if (req.method == "OPTIONS" && req.headers.origin) {
                res.headers["Access-Control-Allow-Origin"] = req.headers.origin;
            }
            next();
        });

        server.use("/", express.static(path.join(path.resolve(__dirname, ".."), "webclient"), {index: "app.xhtml"}));

        /**
         * API
         */

        server.post("/api/excel", export2Excel);

        server.post("/api/calculate", async (req, res) => {
            const loanAmount = req.body.loanAmount || 0;
            const promotionMonth = req.body.promotionMonth || 0;
            const promotionInterest = req.body.promotionInterest || 0;
            const loanMonth = req.body.loanMonth || 0;
            const loanInterest = req.body.loanInterest || 0;
            let interuptMonth = req.body.interuptMonth || 0;
            let totalInterestAtInteruptMonth = 0;

            let details = [{
                monthyAmount: 0,
                remainingAmount: loanAmount,
                interest: 0,
                monthlyInterest: 0,
                totalEachMonth: 0,
            }];
            let monthyAmount = parseFloat((loanAmount / (promotionMonth + loanMonth)).toFixed(2));
            let remainingAmount = loanAmount;
            let totalInterest = 0;

            for (let index = 0; index < promotionMonth; index++) {
                let monthlyInterest = parseFloat(((remainingAmount * (promotionInterest / 100)) / 12).toFixed(2));
                details.push({
                    monthyAmount: monthyAmount,
                    remainingAmount: parseFloat((remainingAmount - monthyAmount).toFixed(2)),
                    interest: promotionInterest,
                    monthlyInterest: monthlyInterest,
                    totalEachMonth: monthyAmount + monthlyInterest,
                });
                remainingAmount -= monthyAmount;
                totalInterest += monthlyInterest;

                if (interuptMonth > 0) {
                    totalInterestAtInteruptMonth += monthlyInterest;
                }
                interuptMonth -= 1;
            }

            for (let index = 0; index < loanMonth; index++) {
                let monthlyInterest = parseFloat(((remainingAmount * (loanInterest / 100)) / 12).toFixed(2));
                details.push({
                    monthyAmount: monthyAmount,
                    remainingAmount: parseFloat((remainingAmount - monthyAmount).toFixed(2)),
                    interest: loanInterest,
                    monthlyInterest: parseFloat(monthlyInterest),
                    totalEachMonth: monthyAmount + parseFloat(monthlyInterest),
                });
                remainingAmount -= monthyAmount;
                totalInterest += monthlyInterest;

                if (interuptMonth > 0) {
                    totalInterestAtInteruptMonth += monthlyInterest;
                }
                interuptMonth -= 1;
            }

            res.json({
                loanAmount: loanAmount,
                totalLoanMonth: promotionMonth + loanMonth,
                totalInterestAmount: totalInterest,
                totalAmount: loanAmount + totalInterest,
                totalInterestAtInteruptMonth: totalInterestAtInteruptMonth,
                details: details
            });
        });

        var port = 10000;
        server.listen(port);
        console.log(`Web server started on port ${port}`);
    }

    module.exports = exportedServer;
})();
